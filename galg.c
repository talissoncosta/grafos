// A C / C++ program for Dijkstra's single source shortest path algorithm.
// The program is for adjacency matrix representation of the graph

#include <stdio.h>
#include <limits.h>
#include <stdbool.h>
#include <stddef.h>

// Number of vertices in the graph
#define V 5

// void greedy1(int graph, int src){
//     int r,c,min,aux,min_r,min_c;
//     min = 0;
//     r = src;
//     while(r!=src){
//         for(c=0;c<V;c++){
//             printf("V [%d][%d] = %d\n",r,c,graph[r][c]);
//             if(graph[r][c] <= min && graph[r][c] > 0){
//                 min = graph[r][c];
//                 min_r = r;
//                 min_c = c;
//             }            
            
//         }
//         r = min_c;
        
//         min = 0;
        
//     }
// }
int checkStack(int *stack, int val){
    for(int i=0;i<V;i++){
        if(stack[i]==val)
            return 1;
    }
    return 0;
}
int main(){

int graph[V][V] = {{0,30,-15,0,20},
                    {-30,0,-10,50,0},
                    {15,10,0,5,-10},
                    {0,50,-5,0,-30},
                    {-20,0,10,30,0}
                    };
        
        // greedy(graph, 0);
        int visited[V];
        int top=0;
        int r,c,min,aux,min_r,min_c,src,sum;
        sum = 0;
        src = 0;
        r = src;
        min = 0;
        while(true){
            visited[top++] = r;  
            
            for(c=0;c<V;c++){
                if(graph[r][c] > 0){
                    aux =  graph[r][c];

                    
                    if(min==0 || aux < min){
                        min = aux;
                        min_r = r;
                        min_c = c;

                        
                    }
                    
                    
          
                    
                }             
                //printf("V [%d][%d] = %d\n",r,c,graph[r][c].val);
                
            }
            if(top==V) break;

            sum+=min;
            //printf("SumF -> %d\n",sum);  
            if(checkStack(visited,min_c)==0)
                r = min_c;
            
            
            min = 0;

        }

        for(int count=0;count<top;count++){
            printf("Vertice(%d)\n",visited[count]);            
        }
        printf("Sum total path -> %d\n",sum);  
        // printf("Top %d\n",top);
        

	return 0;
}

// A C / C++ program for Dijkstra's single source shortest path algorithm.
// The program is for adjacency matrix representation of the graph

#include <stdio.h>
#include <limits.h>
#include <stdbool.h>
#include <stddef.h>

// Number of vertices in the graph
#define V 5
#define E 8

// void greedy1(int graph, int src){
//     int r,c,min,aux,min_r,min_c;
//     min = 0;
//     r = src;
//     while(r!=src){
//         for(c=0;c<V;c++){
//             printf("V [%d][%d] = %d\n",r,c,graph[r][c]);
//             if(graph[r][c] <= min && graph[r][c] > 0){
//                 min = graph[r][c];
//                 min_r = r;
//                 min_c = c;
//             }            
            
//         }
//         r = min_c;
        
//         min = 0;
        
//     }
// }
int checkStack(int *stack, int val){
    for(int i=0;i<V;i++){
        if(stack[i]==val)
            return 1;
    }
    return 0;
}
int main(){

int graph[V][E] = {{20,30,-15,0,0,0,0,0},
                    {0,-30,0,0,-10,0,0,50},
                    {0,0,15,-10,10,0,5,0},
                    {0,0,0,0,0,-30,-5,50},
                    {-20,0,0,10,0,30,0,0}
                    };
        
        // greedy(graph, 0);
        int visited[V];
        int top=0;
        int r,c,min,aux,min_r,min_c,src,sum,aux_r;
        sum = 0;
        src = 0;
        r = src;
        min = 0;
        while(true){
            //printf("ROW %d\n\n",r);

            visited[top++] = r;  
            
            for(c=0;c<E;c++){
                if(graph[r][c] > 0){
                    aux =  graph[r][c];

                    
                    if(min==0 || aux < min){
                        min = aux;
                        min_r = r;
                        min_c = c;

                        
                    }
                    
                    
          
                    
                }            
                
            }
            
            for(aux_r=0;aux_r<V;aux_r++){
               if(graph[aux_r][min_c]!= 0  && aux_r != min_r && checkStack(visited,aux_r)==0){
                   r = aux_r;
               }

            }

            
            //printf("SumF -> %d\n",sum);  
            if(top==V) break;
            sum+=min;
            min = 0;
            c = min_c;
            //r = min_c;
            

        }

        for(int count=0;count<top;count++){
            printf("Vertice(%d)\n",visited[count]);            
        }
        printf("Sum total path -> %d\n",sum);  
        // printf("Top %d\n",top);
        

	return 0;
}
